# S4N Technical Assestment

The main class for running the application is `RoutesManager.java`. 

The input files should have the structure `inXX.txt`. where `XX` is a number starting at `01` and those files should be located under `src/main/resources` folder so keep in mind if a new file is added you'll need to rebuild the project in order for it to force-reload those files.

The output files will have the same structure as input, they will look like `outXX.txt` each file corresponds to a vehicle processing multiple routes.

That's it! Have fun!