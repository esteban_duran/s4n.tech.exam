package solution;

import solution.repository.RoutesReader;
import solution.service.impl.TransportPool;
import solution.utils.Constants;

public class RoutesManager {

	private TransportPool transportPool;

	public RoutesManager(TransportPool transportPool) {
		this.transportPool = transportPool;
	}

	public void startDelivery() {
		this.transportPool.startDelivery();
	}

	public static void main(String[] args) {
		RoutesReader reader = RoutesReader.getInstance();
		TransportPool pool = TransportPool.getInstance();
		pool.setPoolSize(Constants.MAX_DRONES_ONLINE);
		pool.setPendingRoutes(reader.readAllRoutes());

		RoutesManager manager = new RoutesManager(pool);
		manager.startDelivery();
	}

}
