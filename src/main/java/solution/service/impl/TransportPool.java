package solution.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;

import lombok.Getter;
import lombok.Setter;
import solution.exceptions.NoAvailableVehicleException;
import solution.exceptions.ServiceException;
import solution.exceptions.handler.ExceptionHandler;
import solution.model.Coordinate;
import solution.model.Drone;
import solution.model.Route;
import solution.model.Vehicle;
import solution.utils.Constants;
import solution.utils.Direction;

public class TransportPool {

	private static TransportPool pool;

	@Getter
	private int poolSize;

	@Getter
	@Setter
	private int freeVehicles;

	@Getter
	private List<Vehicle> vehicles;

	@Getter
	private Queue<Route[]> pendingRoutes;

	@Getter
	private int deliveredAmount;

	private int vehicleCounter;

	private TransportPool() {
		this.poolSize = Constants.MINUS_ONE;
		this.deliveredAmount = Constants.ZERO;
		this.vehicles = new ArrayList<Vehicle>();
		this.pendingRoutes = new LinkedList<Route[]>();
		this.vehicleCounter = 0;
	}

	public static TransportPool getInstance() {
		if (Objects.isNull(pool)) {
			pool = new TransportPool();
		}
		return pool;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
		this.freeVehicles = poolSize;
	}

	public void setPendingRoutes(List<Route[]> pendingRoutes) {
		if (CollectionUtils.isNotEmpty(pendingRoutes)) {
			pendingRoutes.forEach(this.pendingRoutes::add);
		}
	}

	private Vehicle createVehiclesOnDemand() {
		if (Constants.MAX_DRONES_ONLINE >= freeVehicles && Constants.MAX_DRONES_ONLINE > this.vehicles.size()) {
			Vehicle vehicle = new Drone(++this.vehicleCounter);
			this.vehicles.add(vehicle);
			return vehicle;
		}
		ExceptionHandler.handleQuietException(new NoAvailableVehicleException());
		return null;
	}

	private synchronized Vehicle getFreeVehicle() {
		Optional<Vehicle> optVehicle = this.vehicles.stream().filter(v -> !v.isBusy()).findFirst();

		if (optVehicle.isPresent()) {
			Vehicle vehicle = optVehicle.get();
			vehicle.setCurrentCoord(new Coordinate(Constants.ZERO, Constants.ZERO, Direction.NORTE));
			return vehicle;
		}
		return this.createVehiclesOnDemand();
	}

	public void transport(Vehicle vehicle, Route[] routes) {
		vehicle.setRoutes(routes);
		TransportThread thread;
		thread = new TransportThread(vehicle);
		thread.start();
		this.occupyVehicle();
	}

	public void startDelivery() {
		while (!this.pendingRoutes.isEmpty()) {
			Vehicle vehicle = this.getFreeVehicle();
			Route[] routes = this.pendingRoutes.poll();
			this.transport(vehicle, routes);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				throw new ServiceException(e);
			}
		}
	}

	public void finishRoute(Route[] route) {
		this.freeVehicle();
		this.deliveredAmount++;
	}

	private void freeVehicle() {
		this.freeVehicles++;
	}

	public void occupyVehicle() {
		this.freeVehicles--;
	}
}
