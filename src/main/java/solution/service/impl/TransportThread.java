package solution.service.impl;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import solution.exceptions.ServiceException;
import solution.model.TransportVehicle;
import solution.model.Vehicle;
import solution.repository.ReportWriter;
import solution.utils.Constants;

@Getter
@Setter
public class TransportThread extends Thread {

	private final TransportVehicle vehicle;
	private String report;

	private ReportWriter writer;

	public TransportThread(Vehicle vehicle) {
		this.vehicle = vehicle;
		this.report = Constants.REPORT_HEADER;
		this.writer = new ReportWriter();
	}

	@Override
	public void run() {
		this.vehicle.setBusy(true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			throw new ServiceException(e);
		}
		this.report += this.vehicle.deliver();
		this.writer.writeReport(this.vehicle.getVehicleId(), report);
		this.vehicle.setBusy(false);
		this.writer = null;
		TransportPool.getInstance().finishRoute(vehicle.getRoutes());
	}

}
