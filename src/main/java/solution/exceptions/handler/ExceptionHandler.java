package solution.exceptions.handler;

import java.util.Objects;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExceptionHandler {

	private static ExceptionHandler instance;

	private ExceptionHandler() {

	}

	public static ExceptionHandler getInstance() {
		if (Objects.isNull(instance)) {
			instance = new ExceptionHandler();
		}
		return instance;
	}

	public static void handleQuietException(Throwable exc) {
		log.warn(exc.getMessage(), exc);
	}
}
