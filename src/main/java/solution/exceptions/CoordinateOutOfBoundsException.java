package solution.exceptions;

public class CoordinateOutOfBoundsException extends ServiceException{

	private static final long serialVersionUID = -2253428174994923390L;
	public static final String DEFAULT_MSG = "The route is out of boundaries, please input a different one.";

	public CoordinateOutOfBoundsException(String cause) {
		super(cause);
	}
	
	public CoordinateOutOfBoundsException() {
		super(DEFAULT_MSG);
	}
}
