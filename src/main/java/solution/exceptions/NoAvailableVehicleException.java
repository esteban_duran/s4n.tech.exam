package solution.exceptions;

public class NoAvailableVehicleException extends ServiceException {

	private static final long serialVersionUID = 3626082094099137969L;
	public static final String DEFAULT_MSG = "No available drones currently!";

	public NoAvailableVehicleException(String cause) {
		super(cause);
	}

	public NoAvailableVehicleException() {
		super(DEFAULT_MSG);
	}

}
