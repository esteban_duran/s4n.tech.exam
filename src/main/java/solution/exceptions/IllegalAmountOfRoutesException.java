package solution.exceptions;

import solution.utils.Constants;

public class IllegalAmountOfRoutesException extends ServiceException {

	private static final String MESSAGE_FORMAT = "The amount of routes [%s] is bigger than the max routes allowed per vehicle [%s]";
	private static final long serialVersionUID = 2683215518245451228L;

	public IllegalAmountOfRoutesException(long max) {
		super(String.format(MESSAGE_FORMAT, max, Constants.MAX_ROUTES_PER_DRONE));
	}
}
