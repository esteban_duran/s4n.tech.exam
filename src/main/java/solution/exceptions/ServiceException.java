package solution.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 1316712992111204002L;

	public ServiceException(String cause, Throwable t) {
		super(cause, t);
		log.error(cause, t);
	}

	public ServiceException(String cause) {
		super(cause);
		log.error(cause);
	}

	public ServiceException(Throwable t) {
		super(t);
		log.error(t.getMessage(), t);
	}

}
