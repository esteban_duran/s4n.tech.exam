package solution.model;

public interface TransportVehicle {

	String deliver();

	void setBusy(boolean b);
	
	boolean isBusy();
	
	Integer getVehicleId();
	
	Route[] getRoutes();
}
