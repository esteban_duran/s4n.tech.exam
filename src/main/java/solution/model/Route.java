package solution.model;

import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import solution.exceptions.CoordinateOutOfBoundsException;
import solution.exceptions.handler.ExceptionHandler;
import solution.utils.Constants;
import solution.utils.Direction;
import solution.utils.Position;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Route {

	private String id;
	private String path;

	public boolean isWithinBoundaries(Route route) {
		boolean isValidRoute = true;
		if (Objects.nonNull(route)) {
			Coordinate startCoord = new Coordinate(Constants.ZERO, Constants.ZERO, Direction.NORTE);
			Position pos;
			for (int i = 0; i < route.getPath().length() && isValidRoute; i++) {
				pos = Position.fromChar(route.getPath().charAt(i));
				startCoord.move(pos);
				if (!startCoord.isCoordWithinBoundaries()) {
					ExceptionHandler.handleQuietException(new CoordinateOutOfBoundsException());
					isValidRoute = false;
				}
			}
		} else {
			isValidRoute = false;
		}
		return isValidRoute;
	}

	public String doRoute(Vehicle vehicle, Route route) {
		StringBuilder builder = new StringBuilder();
		if (isWithinBoundaries(route)) {
			Position pos;
			Coordinate coord = vehicle.getCurrentCoord();
			for (int i = 0; i < route.getPath().length(); i++) {
				pos = Position.fromChar(route.getPath().charAt(i));
				coord.move(pos);
				vehicle.setCurrentCoord(coord);
				builder.append(String.format(Constants.ROUTE_MOVEMENT_FORMAT, coord, coord.getDirection()));
			}
		} else {
			builder.append(String.format(Constants.OUT_OF_BOUNDARIES_ROUTE_FORMAT, route));
		}
		return builder.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Route)) {
			return false;
		}
		Route other = (Route) obj;
		if (path == null) {
			if (other.path != null) {
				return false;
			}
		} else if (!path.equals(other.path)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Route [id=" + id + ", path=" + path + "]";
	}
}
