package solution.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Drone extends Vehicle {

	public Drone(int id) {
		super(id);
	}
}
