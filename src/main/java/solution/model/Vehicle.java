package solution.model;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;
import lombok.Setter;
import solution.utils.CommonsUtils;
import solution.utils.Constants;
import solution.utils.Direction;

public abstract class Vehicle implements TransportVehicle {

	private final Integer id;
	private boolean busy;

	@Getter
	@Setter
	private Route[] routes;

	@Getter
	@Setter
	private Coordinate currentCoord;

	public Vehicle(int id) {
		this.id = id;
		currentCoord = new Coordinate(Constants.ZERO, Constants.ZERO, Direction.NORTE);
	}

	@Override
	public String deliver() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < this.routes.length; i++) {
			Route route = this.routes[i];
			builder.append(String.format(Constants.ROUTE_SEPARATOR, route.getId(), CommonsUtils.ellipsis(route.getPath())));
			builder.append(route.doRoute(this, route) + StringUtils.LF);
		}
		return builder.toString();
	}

	@Override
	public synchronized void setBusy(boolean b) {
		this.busy = b;
	}

	@Override
	public synchronized boolean isBusy() {
		return this.busy;
	}

	@Override
	public Integer getVehicleId() {
		return this.id;
	}

	@Override
	public Route[] getRoutes() {
		return this.routes;
	}

}
