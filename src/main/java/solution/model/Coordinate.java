package solution.model;

import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import solution.utils.Constants;
import solution.utils.Direction;
import solution.utils.Position;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Coordinate {

	private Integer x;
	private Integer y;
	private Direction direction;

	@Override
	public String toString() {
		return String.format(Constants.COORDINATE_FORMAT, x, y);
	}

	public boolean isEmpty() {
		return Objects.isNull(x) && Objects.isNull(y) && Objects.isNull(direction);
	}

	public Coordinate getCoordFromPosition(Position pos) {
		Coordinate coord = new Coordinate(0, 0, this.direction);
		switch (this.direction) {
		case NORTE:
			this.getMoveCoordNorth(coord, pos);
			break;
		case OESTE:
			this.getMoveCoordWest(coord, pos);
			break;
		case ORIENTE:
			this.getMoveCoordEast(coord, pos);
			break;
		case SUR:
			this.getMoveCoordSouth(coord, pos);
			break;
		}

		return coord;
	}

	private void getMoveCoordNorth(Coordinate coord, Position pos) {
		if (Objects.nonNull(coord) && Position.isValid(pos)) {
			switch (pos) {
			case A:
				coord.setY(Constants.ONE);
				break;
			case I:
				coord.setX(Constants.MINUS_ONE);
				coord.setDirection(Direction.OESTE);
				break;
			case D:
				coord.setX(Constants.ONE);
				coord.setDirection(Direction.ORIENTE);
				break;
			}
		}
	}

	private void getMoveCoordEast(Coordinate coord, Position pos) {
		if (Objects.nonNull(coord) && Position.isValid(pos)) {
			switch (pos) {
			case A:
				coord.setX(Constants.ONE);
				break;
			case I:
				coord.setY(Constants.ONE);
				coord.setDirection(Direction.NORTE);
				break;
			case D:
				coord.setY(Constants.MINUS_ONE);
				coord.setDirection(Direction.SUR);
				break;
			}
		}
	}

	private void getMoveCoordWest(Coordinate coord, Position pos) {
		if (Objects.nonNull(coord) && Position.isValid(pos)) {
			switch (pos) {
			case A:
				coord.setX(Constants.MINUS_ONE);
				break;
			case I:
				coord.setY(Constants.MINUS_ONE);
				coord.setDirection(Direction.SUR);
				break;
			case D:
				coord.setY(Constants.ONE);
				coord.setDirection(Direction.NORTE);
				break;
			}
		}
	}

	private void getMoveCoordSouth(Coordinate coord, Position pos) {
		if (Objects.nonNull(coord) && Position.isValid(pos)) {
			switch (pos) {
			case A:
				coord.setY(Constants.MINUS_ONE);
				break;
			case I:
				coord.setX(Constants.ONE);
				coord.setDirection(Direction.ORIENTE);
				break;
			case D:
				coord.setX(Constants.MINUS_ONE);
				coord.setDirection(Direction.OESTE);
				break;
			}
		}
	}

	public void move(Position pos) {
		if (Objects.nonNull(pos)) {
			this.addCoords(this.getCoordFromPosition(pos));
		}
	}

	private void addCoords(Coordinate two) {
		if (Objects.nonNull(two)) {
			this.setX(this.getX() + two.getX());
			this.setY(this.getY() + two.getY());
			this.setDirection(two.getDirection());
		}
	}

	public boolean isCoordWithinBoundaries() {
		if (Objects.nonNull(this)) {
			return (Math.pow(this.x, Constants.POWER_OF_2)
					+ Math.pow(this.y, Constants.POWER_OF_2)) <= Constants.MAX_ROUND_BLOCKS;
		}
		return Boolean.FALSE;
	}
}
