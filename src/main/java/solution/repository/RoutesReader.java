package solution.repository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;
import solution.exceptions.IllegalAmountOfRoutesException;
import solution.exceptions.ServiceException;
import solution.model.Route;
import solution.utils.CommonsUtils;
import solution.utils.Constants;

public class RoutesReader {

	private static RoutesReader instance;

	@Getter
	private List<Route[]> allRoutes;

	private RoutesReader() {
		this.allRoutes = new ArrayList<Route[]>();
	}

	public static RoutesReader getInstance() {
		if (Objects.isNull(instance)) {
			instance = new RoutesReader();
		}
		return instance;
	}

	/**
	 * Reads and parses the routes from plain text file into a route array.
	 * 
	 * @param drone
	 * @return
	 */
	public Route[] readRoutes(int id) {
		final String path = String.format(Constants.FILE_PATH_FORMAT, Constants.ROUTES_PATH,
				Constants.ROUTE_FILE_PREFIX, CommonsUtils.getFileId(id));
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream(path)))) {
			String line = reader.readLine();
			final List<Route> routes = new ArrayList<Route>();
			int index = 1;
			while (StringUtils.isNotBlank(line)) {
				routes.add(new Route(String.format(Constants.ROUTE_ID_PREFIX, CommonsUtils.getFileId(id)),
						line));
				if (Constants.MAX_ROUTES_PER_DRONE < index) {
					throw new IllegalAmountOfRoutesException(index);
				}
				index++;
				line = reader.readLine();
			}
			return routes.toArray(new Route[] {});
		} catch (FileNotFoundException e) {
			throw new ServiceException(e);
		} catch (IOException e1) {
			throw new ServiceException(e1);
		}
	}

	public List<Route[]> readAllRoutes() {
		ClassLoader classLoader = ClassLoader.getSystemClassLoader();
		final String path = new File(classLoader.getResource(Constants.ROUTES_PATH).getFile()).getAbsolutePath();
		try (Stream<Path> files = Files.list(Paths.get(path))) {
			long count = files.count();
			for (int i = 1; i < count; i++) {
				this.allRoutes.add(this.readRoutes(i));
			}
		} catch (IOException e) {
			throw new ServiceException(e);
		}
		return this.allRoutes;
	}
}
