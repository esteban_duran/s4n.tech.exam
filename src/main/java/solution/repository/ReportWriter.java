package solution.repository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import solution.exceptions.ServiceException;
import solution.utils.CommonsUtils;
import solution.utils.Constants;

public class ReportWriter {

	public ReportWriter() {
	}

	public synchronized void writeReport(int id, String report) {
		final String path = String.format(Constants.FILE_PATH_FORMAT, Constants.REPORTS_PATH,
				Constants.REPORT_FILE_PREFIX, CommonsUtils.getFileId(id));
		File file = new File(path);
		try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true)))) {
			writer.write(report);
			writer.flush();
		} catch (FileNotFoundException e) {
			throw new ServiceException(e);
		} catch (IOException e1) {
			throw new ServiceException(e1);
		}
	}

}
