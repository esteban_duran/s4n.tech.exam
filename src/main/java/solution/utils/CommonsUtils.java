package solution.utils;

import org.apache.commons.lang3.StringUtils;

public class CommonsUtils {

	private CommonsUtils() {

	}

	public static String getFileId(int id) {
		return id < 10 ? (StringUtils.EMPTY + Constants.ZERO) + id : StringUtils.EMPTY + id;
	}

	public static String ellipsis(String routePath) {
		String ellipsis = "";
		if (StringUtils.isNotBlank(routePath)) {
			ellipsis = routePath.length() > Constants.MAX_PATH_DISPLAY_CHARS ? routePath
					.substring(Constants.ZERO, Constants.MAX_PATH_DISPLAY_CHARS - Constants.THREE_DOTS.length())
					.concat(Constants.THREE_DOTS) : routePath;
		}
		return ellipsis;
	}
}
