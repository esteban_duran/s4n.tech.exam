package solution.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;

public class Constants {

	public static final String RESOURCES_PATH = "src/main/resources/";
	public static final String ROUTES_PATH = "routes/";
	public static final String REPORTS_PATH = RESOURCES_PATH + "reports/";
	public static final String ROUTE_FILE_PREFIX = "in";
	public static final String REPORT_FILE_PREFIX = "out";
	public static final String FILE_EXTENSION = ".txt";
	public static final String FILENAME_FORMAT = "%s%s" + FILE_EXTENSION;
	public static final String FILE_PATH_FORMAT = "%s/" + FILENAME_FORMAT;

	public static final String COORDINATE_FORMAT = "(%s,%s)";
	public static final String ROUTE_MOVEMENT_FORMAT = "%-10s direcci�n %s \n";

	public static final String OUT_OF_BOUNDARIES_ROUTE_FORMAT = "The  %s is out of boundaries and cannot be delivered, please adjust it.";

	public static final int MAX_ROUTES_PER_DRONE = 3;
	public static final int MAX_ROUND_BLOCKS = 10;
	public static final int MAX_DRONES_ONLINE = 20;

	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int MINUS_ONE = -1;
	public static final int POWER_OF_2 = 2;

	public static final String ROUTE_ID_PREFIX = "RT_%s";

	public static final int MAX_PATH_DISPLAY_CHARS = 10;
	public static final String THREE_DOTS = "...";

	public static final String SIMPLE_DATE_FORMAT = "dd/MM/yyyy@hh:mm:ss";
	public static final String REPORT_HEADER = "== Reporte de entregas ==\t"
			+ LocalDateTime.now().format(DateTimeFormatter.ofPattern(SIMPLE_DATE_FORMAT)) + StringUtils.LF;
	public static final String ROUTE_SEPARATOR = "\t== ROUTE %s [%s] ===" + StringUtils.LF;

	private Constants() {

	}
}
