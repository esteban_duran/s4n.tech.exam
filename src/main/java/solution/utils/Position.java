package solution.utils;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum Position {

	A('A'), I('I'), D('D');

	char name;

	private Position(char name) {
		this.name = name;
	}

	public static boolean isValid(Position pos) {
		if (Objects.isNull(pos)) {
			return Boolean.FALSE;
		} else {
			return Stream.of(Position.values()).anyMatch(p -> p.name == pos.name);
		}
	}

	public static Position fromChar(char c) {
		Optional<Position> optPos = Optional.empty();
		optPos = Stream.of(Position.values()).filter(p -> p.name == c).findFirst();
		if(optPos.isPresent()) {
			return optPos.get();
		}
		return null;
	}
}
