package solution.utils;

import java.util.Objects;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

public enum Direction {

	NORTE("N"), OESTE("W"), ORIENTE("E"), SUR("S");

	String name;

	private Direction(String name) {
		this.name = name;
	}

	public boolean isValid(Position direction) {
		if (Objects.isNull(direction)) {
			return Boolean.FALSE;
		} else {
			return Stream.of(Direction.values()).anyMatch(d -> d.name.equals(direction.name));
		}
	}

	@Override
	public String toString() {
		return super.toString().substring(Constants.ZERO, Constants.ONE).toUpperCase()
				.concat(super.toString().substring(Constants.ONE, super.toString().length()).toLowerCase());
	}
}
