package solution.test.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import solution.model.Drone;
import solution.model.Route;
import solution.model.Vehicle;

@RunWith(JUnit4.class)
public class RouteTest {

	Vehicle vehicle;

	Route base;

	@Before

	public void setUp() throws Exception {
		vehicle = new Drone(1);
	}

	@Test
	public final void testIsWithinBoundaries_emptyRoute() {
		this.base = new Route("1", "");
		Assert.assertTrue(base.isWithinBoundaries(base));
	}

	@Test
	public final void testIsWithinBoundaries_nullRoute() {
		this.base = new Route("1", "");
		Assert.assertFalse(base.isWithinBoundaries(null));
	}

	@Test
	public final void testIsWithinBoundaries_validRoute() {
		this.base = new Route("1", "AAI");
		Assert.assertTrue(base.isWithinBoundaries(base));
	}

	@Test
	public final void testIsWithinBoundaries_invalidRoute() {
		this.base = new Route("1", "AAIIDDAA");
		Assert.assertFalse(base.isWithinBoundaries(base));
	}

	@Test
	public final void testDoRoute() {
		this.base = new Route("1", "AAI");
		String expected = "(0,1)      direcci�n Norte \n(0,2)      direcci�n Norte \n(-1,2)     direcci�n Oeste \n";
		Assert.assertEquals(expected, base.doRoute(vehicle, base));
	}
}